package main

import (
	"flag"
	"fmt"

	"github.com/docopt/docopt-go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/codref/breach-scanner/config"
	"gitlab.com/codref/breach-scanner/model"
	"gitlab.com/codref/breach-scanner/scanner"
	"gitlab.com/codref/breach-scanner/util"
)

// Subversion engraved on build script
var Subversion = "development"

// Version engraved on build script
var Version = "development"

func main() {
	usage := `Offline Breach Scanner.

Usage:
  breach-scanner scan list
  breach-scanner hash index <path> --scanID <scanID>
  breach-scanner hash compare <suspicious-scanID> <verbatim-scanID> --diff <diffFilePath> [--exclude=<exclude>]
  breach-scanner modules index <path> --scanID <scanID>
  breach-scanner modules download <path> --archivePoolPath <archivePoolPath> --scanID <scanID>
  breach-scanner mysqldump split <path> --outputPath=<outputPath>
  breach-scanner mysqldump unserialize php <path> --outputPath=<outputPath>
  breach-scanner log index <logFilePath> --scanID <scanID> --format=<logFormat>
  breach-scanner unserialize php <text>
  breach-scanner cleanup	
  breach-scanner -h | --help
  breach-scanner --version

Options:
  --outputPath=<outputPath> -o <outputPath>      Output path
  --exclude=<exclude> -e <exclude>               Exclude file
  --filePath -f									 File to parse
  -h --help	                                     Show this screen.
  -c <configFile>								 Configuration file
  --version                                      Show version.`

	arguments, _ := docopt.ParseDoc(usage)

	log.Printf("EvoUser ShellAPI v2. Version %s.%s", Version, Subversion)
	var path2config string
	flag.StringVar(&path2config, "c", "./breach-scanner.yml", "define path to application config file")
	flag.Parse()

	log.Debugf("Loading config from %s...", path2config)

	err := config.LoadConfig(path2config)
	if err != nil {
		log.Fatalf("%s : while loading config from %s", err, path2config)
	}
	log.Infof("Config loaded from %s.", path2config)

	log.Info("Initializing support database...")
	err = model.ConnectToDatabase(config.Config.DatabasePath + "/breach-scanner.db")
	if err != nil {
		log.Fatalf("%s : while dialing sqlite database %s", err, "./breach-scanner.db")
	}
	log.Info("Database connection established!")
	defer model.DB.Close()

	log.Info("Fixing database structure...")
	err = model.FixDatabaseStructure()
	if err != nil {
		log.Fatalf("%s : while fixing databsase structure", err)
	}
	log.Info("Database structure fixed!")

	// Set or retrieve scanID
	var scan model.Scan
	if arguments["--scanID"].(bool) {
		scan = model.CreateScanID(arguments["<scanID>"].(string))
		log.Infof("Scan %s has now ID=%d, use it in case you are going to perform manual queries.", arguments["<scanID>"].(string), scan.ID)
	}

	if arguments["scan"].(bool) {
		if arguments["list"].(bool) {
			scanner.PrintAvailableScans()
		}
	} else if arguments["cleanup"].(bool) {
		log.Info("Purging deleted rows on DB...")
		model.DB.Unscoped().Delete(&model.Hash{})
		model.DB.Unscoped().Delete(&model.Module{})
		model.DB.Unscoped().Delete(&model.Scan{})
		log.Info("Done")
	} else if arguments["hash"].(bool) {
		if arguments["index"].(bool) {
			scan.HashIndexPath = arguments["<path>"].(string)
			model.DB.Save(&scan)
			scanner.HashFilesystem(scan.ID, arguments["<path>"].(string))
		} else if arguments["compare"].(bool) {
			excludePath := ""
			if excludeArgument, ok := arguments["--exclude"]; ok && excludeArgument != nil {
				excludePath = excludeArgument.(string)
			}
			scanner.CompareFilesystem(arguments["<suspicious-scanID>"].(string), arguments["<verbatim-scanID>"].(string), arguments["<diffFilePath>"].(string), excludePath)
		}
	} else if arguments["modules"].(bool) {
		if arguments["index"].(bool) {
			scan.ModuleIndexPath = arguments["<path>"].(string)
			model.DB.Save(&scan)
			scanner.IndexDrupalModules(scan.ID, arguments["<path>"].(string))
		} else if arguments["download"].(bool) {
			scanner.DownloadDruplaModules(scan.ID, arguments["<path>"].(string), arguments["<archivePoolPath>"].(string))
		}
	} else if arguments["mysqldump"].(bool) {
		if arguments["split"].(bool) {
			scanner.SplitMysqldumpFile(arguments["<path>"].(string), arguments["--outputPath"].(string))
		} else if arguments["unserialize"].(bool) && arguments["php"].(bool) {
			scanner.UnserializePHPString(arguments["<path>"].(string), arguments["--outputPath"].(string))
		}
	} else if arguments["log"].(bool) {
		if arguments["index"].(bool) {
			scanner.IndexPleskAccessLogs(scan.ID, arguments["<logFilePath>"].(string))
		}
	} else if arguments["unserialize"].(bool) {
		if arguments["php"].(bool) {
			fmt.Println(util.StringUnserializePHP(arguments["<text>"].(string)))
		}
	}

}
