export semver=$(shell cat ./VERSION)
export arch=$(shell uname)-$(shell uname -m)
export gittip=$(shell git log --format='%h' -n 1)
export ver=$(semver).$(gittip).$(arch)
export ver_windows=$(semver).$(gittip).Windows-x86_64
export subver=$(shell hostname)_on_$(shell date -u '+%Y-%m-%d_%I:%M:%S%p')


all: build

run: 
	go run main.go

start: run


deps:
	# install all dependencies required for running application
	go version
	go env

	# installing golint code quality tools and checking, if it can be started
	cd ~ && go get -u golang.org/x/lint/golint
	golint

	# installing tool for packing tempates for adminUI, and checking, if it can be started
	rm -rf ~/go/bin/go-bindata
	cd ~ && go get -u github.com/go-bindata/go-bindata/...
	go-bindata -version

	# installing golang dependencies using golang modules
	go mod tidy # ensure go.mod is sane
	go mod verify # ensure dependencies are present

lint:
	# run basic code quality and sanity checks, alongside with unit tests
	gofmt -w=true -s=true -l=true main.go
	golint ./...
	go vet ./...

check: lint
	# ran unit tests with coverage report
	go test -v -coverprofile=cover.out ./...

build_prod: clean deps check
	# build production grade binary
	GOOS=linux GOARCH=amd64 go build -ldflags "-X main.Subversion=$(subver) -X main.Version=$(ver)" -o "build/breach-scanner-$(ver)" main.go
	GOOS=windows GOARCH=amd64 CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc CXX=x86_64-w64-mingw32-g++ go build -ldflags "-X main.Subversion=$(subver) -X main.Version=$(ver_windows)" -o "build/breach-scanner-$(ver_windows).exe" main.go

build_dev: clean deps check
	# build development grade binary as `shellapi-dev` binary executable
	go build -o "breach-scanner-dev" main.go	

clean:
	# Clean removes object files from package source directories.
	go clean	
