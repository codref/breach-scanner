package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Configuration ShellApi server configuration structure
type Configuration struct {
	DatabasePath string `yaml:"database-path"`
	Command      struct {
		FormatSerializedString string `yaml:"format-serialized-string"`
	} `yaml:"command"`
}

var (
	// Config contains the running ShellAPI server configuration
	Config *Configuration
)

// LoadConfig Load the configuration file
func LoadConfig(configFile string) error {
	// Read config file:
	bytes, err := ioutil.ReadFile(configFile)
	if err != nil {
		return err
	}

	// Attempt to unmarshal config file content into Configuration struct:
	if err = yaml.Unmarshal(bytes, &Config); err != nil {
		return err
	}
	return nil
}
