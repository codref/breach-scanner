package util

import (
	"bufio"
	"log"
	"os"
	"regexp"
)

// FileSubmatchAll search using a regexp inside a file
func FileSubmatchAll(regexps map[string]string, path string) map[string][]string {

	result := make(map[string][]string)
	res := make(map[string]*(regexp.Regexp))

	// compile all reg exps
	for k, re := range regexps {
		res[k], _ = regexp.Compile(re)
	}

	// open file
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// parse file line by line
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		for k, r := range res {
			match := r.FindAllStringSubmatch(scanner.Text(), -1)
			if match != nil {
				for _, element := range match {
					result[k] = append(result[k], element[1]) // we always take the first match - no need to go further
				}
			}
		}

		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}

	}
	return result
}

// StringSubmatchAll search using a regexp inside a string
func StringSubmatchAll(regexps map[string]string, text string) map[string][]string {
	result := make(map[string][]string)

	// compile all reg exps and do the match
	for k, re := range regexps {
		r, err := regexp.Compile(re)
		if err != nil {
			log.Fatal(err)
		}
		match := r.FindAllStringSubmatch(text, -1)
		if match != nil {
			for _, element := range match {
				for _, subElement := range element[1:] {
					result[k] = append(result[k], subElement)
				}
			}
		}
	}
	return result
}

// StringMatchAll matches a string for regex
func StringMatchAll(regexps []string, text string) bool {
	// compile all reg exps and do the match
	for _, re := range regexps {
		cre, err := regexp.Compile(re)
		if err != nil {
			log.Fatal(err)
		}
		if cre.MatchString(text) {
			return true
		}
	}
	return false
}

// ReadLines reads a whole file into memory
// and returns a slice of its lines.
func ReadLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}
