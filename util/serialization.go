package util

import (
	"os/exec"
	"strings"

	"gitlab.com/codref/breach-scanner/config"
)

// StringUnserializePHP call a Docker container to unserialize a PHP object string
func StringUnserializePHP(text string) string {
	command := append(strings.Split(config.Config.Command.FormatSerializedString, " "), text)
	cmd := exec.Command(command[0], command[1:]...)
	output, err := cmd.CombinedOutput()
	if err != nil {
		// TODO nothing, it's combined output
	}
	return string(output)
}
