package util

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
)

// CalculateMd5 calculate the MD5 sum of a file
func CalculateMd5(path string) string {
	f, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	h := md5.New()
	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}

	return fmt.Sprintf("%x", h.Sum(nil))
}

// Md5 calc md5 of a string
func Md5(text string) string {
	h := md5.New()
	h.Write([]byte(text))
	return fmt.Sprintf("%x", h.Sum(nil))
}

// DiffFiles calculate the Difference two files
func DiffFiles(outputFilePath string, suspiciousFilePath string, verbatimFilePath string) {
	f, err := os.OpenFile(outputFilePath,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	cmd := exec.Command("diff", "-u", suspiciousFilePath, verbatimFilePath)
	output, err := cmd.CombinedOutput()
	if err != nil {
		// TODO nothing, it's combined output
	}
	if _, err := f.Write(output); err != nil {
		log.Println(err)
	}
}

// LineCounter read line numbers on file
func LineCounter(filePath string) (int, error) {
	// open file
	file, err := os.Open(filePath)
	if err != nil {
		return 0, err
	}
	defer file.Close()

	buf := make([]byte, 32*1024)
	count := 0
	lineSep := []byte{'\n'}

	for {
		c, err := file.Read(buf)
		count += bytes.Count(buf[:c], lineSep)

		switch {
		case err == io.EOF:
			return count, nil

		case err != nil:
			return count, err
		}
	}
}
