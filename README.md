# Drupal Breach Scanner

```text
Usage:
  breach-scanner scan list
  breach-scanner hash index <path> --scanID <scanID>
  breach-scanner hash compare <suspicious-scanID> <verbatim-scanID> --diff <diffFilePath> [--exclude=<exclude>]
  breach-scanner modules index <path> --scanID <scanID>
  breach-scanner modules download <path> --archivePoolPath <archivePoolPath> --scanID <scanID>
  breach-scanner mysqldump split <path> --outputPath=<outputPath>
  breach-scanner mysqldump unserialize php <path> --outputPath=<outputPath>
  breach-scanner log index <logFilePath> --scanID <scanID> --format=<logFormat>
  breach-scanner unserialize php <text>
  breach-scanner cleanup
  breach-scanner -h | --help
  breach-scanner --version

Options:
  --outputPath=<outputPath> -o <outputPath>      Output path
  --exclude=<exclude> -e <exclude>               Exclude file
  --filePath -f                                  File to parse
  -h --help                                      Show this screen.
  -c <configFile>                                Configuration file
  --version                                      Show version.
```