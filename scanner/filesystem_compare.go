package scanner

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/codref/breach-scanner/model"
	"gitlab.com/codref/breach-scanner/util"
)

// CompareFilesystem compares two scans
func CompareFilesystem(suspiciousScanName string, verbatimScanName string, diffFilePath string, excludeFilePath string) {
	// get scan IDs
	suspiciousScan := model.GetScanID(suspiciousScanName)
	verbatimScan := model.GetScanID(verbatimScanName)

	type Compared struct {
		FilePath string
		Hash1    string
		Hash2    string
	}
	var compared []Compared

	// retrieve suspicious files
	result := model.DB.Raw(`
	SELECT suspicious.file_path, suspicious.hash h1, verbatim.hash h2 FROM (
		SELECT * from hashes where scan_id = ?
	) suspicious INNER JOIN (
		SELECT * from hashes where scan_id = ?
	) verbatim ON suspicious.file_path = verbatim.file_path
	WHERE h1 <> h2
	`, suspiciousScan.ID, verbatimScan.ID).Scan(&compared)
	if result.Error != nil {
		log.Errorf("Error: %s", result.Error)
	}
	fmt.Printf("Found %d suspicious files which have different md5 signature\n", len(compared))

	// create DIFF report
	log.Infof("Creating diff report %s", diffFilePath)
	var exclude []string
	var err error
	if excludeFilePath != "" {
		exclude, err = util.ReadLines(excludeFilePath)
		if err != nil {
			log.Fatal(err)
		}
	}
	excluded := 0
	for _, c := range compared {
		if !util.StringMatchAll(exclude, c.FilePath) {
			fmt.Printf("[MD5]   +   %s\n", c.FilePath)
			util.DiffFiles(diffFilePath, suspiciousScan.HashIndexPath+c.FilePath, verbatimScan.HashIndexPath+c.FilePath)
		} else {
			fmt.Printf("[MD5]   -   %s\n", c.FilePath)
			excluded++
		}
	}
	if excludeFilePath != "" {
		log.Infof("Excluding %d results. Diff report contains %d entries. [--exclude=%s]", excluded, len(compared)-excluded, excludeFilePath)
	}
	log.Info("Report created")

	// retrieve not comparable files
	result = model.DB.Raw(`
	SELECT file_path from hashes where scan_id = ?
	EXCEPT
	SELECT file_path from hashes where scan_id = ?
	`, suspiciousScan.ID, verbatimScan.ID).Scan(&compared)
	if result.Error != nil {
		log.Errorf("Error: %s", result.Error)
	}
	fmt.Printf("Cannot compare %d files, they are missing on verbatim filesystem\n", len(compared))

	for _, c := range compared {
		if !util.StringMatchAll(exclude, c.FilePath) {
			fmt.Printf("[M>V]   +   %s\n", c.FilePath)
		} else {
			fmt.Printf("[M>V]   -   %s\n", c.FilePath)
		}
	}

	// retrieve not comparable files
	result = model.DB.Raw(`
	SELECT file_path from hashes where scan_id = ?
	EXCEPT
	SELECT file_path from hashes where scan_id = ?
	`, verbatimScan.ID, suspiciousScan.ID).Scan(&compared)
	if result.Error != nil {
		log.Errorf("Error: %s", result.Error)
	}
	fmt.Printf("Cannot compare %d files, they are missing on suspicious filesystem\n", len(compared))

	for _, c := range compared {
		if !util.StringMatchAll(exclude, c.FilePath) {
			fmt.Printf("[M>S]   +   %s\n", c.FilePath)
		} else {
			fmt.Printf("[M>S]   -   %s\n", c.FilePath)
		}
	}

}
