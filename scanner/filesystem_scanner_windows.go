package scanner

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/codref/breach-scanner/model"
	"gitlab.com/codref/breach-scanner/util"
)

// HashFilesystem traverse all filesystem starting from a specific path
func HashFilesystem(scanID uint, startPath string) {
	// delete previous analysis
	model.DB.Unscoped().Where("scan_id = ?", scanID).Delete(model.Hash{})

	indexed := 0
	log.Infof("Hash calculation of directory %s started...", startPath)
	err := filepath.Walk(startPath,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if !info.IsDir() {
				hash := util.CalculateMd5(path)
				fsEntry := model.Hash{
					ScanID:       scanID,
					FilePath:     strings.TrimPrefix(path, startPath),
					Hash:         hash,
					FileModified: info.ModTime(),
				}
				model.DB.Create(&fsEntry)
				indexed++
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
	log.Infof("Hash calculation completed")
	fmt.Printf("Indexd %d files", indexed)
}
