package scanner

import (
	"fmt"

	"gitlab.com/codref/breach-scanner/model"
)

//PrintAvailableScans lists all available scans
func PrintAvailableScans() {
	var scans []model.Scan
	model.DB.Find(&scans)
	for _, s := range scans {
		fmt.Printf("Scan: %s\n", s.Name)
		fmt.Printf("\tCreated at: %s \tUpdated at: %s\n", s.CreatedAt, s.UpdatedAt)
		fmt.Printf("\tHash Index Path: %s\n", s.HashIndexPath)
		fmt.Printf("\tModule Index Path: %s\n\n", s.ModuleIndexPath)
	}
}
