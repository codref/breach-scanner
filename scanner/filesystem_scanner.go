package scanner

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/antchfx/xmlquery"
	log "github.com/sirupsen/logrus"
	"gitlab.com/codref/breach-scanner/model"
	"gitlab.com/codref/breach-scanner/util"
)

// IndexDrupalModules retrieve a module list
func IndexDrupalModules(scanID uint, startPath string) {
	regexps := map[string]string{
		"project": "^\\s*project\\s*=\\s*(\".*\")\\s*$",
		"version": "^\\s*version\\s*=\\s*(\".*\")\\s*$",
	}

	// delete previous analysis
	model.DB.Unscoped().Where("scan_id = ?", scanID).Delete(model.Module{})

	err := filepath.Walk(startPath,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			ext := filepath.Ext(path)
			moduleName := strings.TrimSuffix(info.Name(), ext) // module name is fetched from folder name
			if ext == ".info" {
				matches := util.FileSubmatchAll(regexps, path)
				if len(matches["project"]) == 0 {
					// this is not a valid Drupal module
					moduleEntry := model.Module{ScanID: scanID, ModulePath: moduleName, IsValid: false}
					model.DB.Create(&moduleEntry)
				} else if len(matches["project"]) > 0 && moduleName == strings.Trim(matches["project"][0], "\"") {
					// this is a valid Drupal module
					version := ""
					if len(matches["version"]) > 0 {
						version = strings.Trim(matches["version"][0], "\"")
					}

					// search Drupal archive for newer versions and download URL
					doc, err := xmlquery.LoadURL(fmt.Sprintf("https://updates.drupal.org/release-history/%s/7.x", moduleName))
					if err != nil {
						return err
					}
					projectName := ""
					projectURL := ""
					downloadLink := ""
					newerVersions := 0
					pendingSecurityPatches := 0
					for _, release := range xmlquery.Find(doc, "/project/releases/release") {
						projectName = release.SelectElement("name").InnerText()
						projectURL = release.SelectElement("release_link").InnerText()
						v := release.SelectElement("version").InnerText()
						if v == version {
							downloadLink = release.SelectElement("download_link").InnerText()
							break
						}
						for _, term := range xmlquery.Find(release, "/terms/term") {
							t := term.SelectElement("value").InnerText()
							if t == "Security update" {
								pendingSecurityPatches++
							}
						}
						newerVersions++
					}

					moduleEntry := model.Module{
						ScanID:                 scanID,
						Name:                   projectName,
						ModulePath:             strings.TrimPrefix(path, startPath),
						IsValid:                true,
						Version:                version,
						ProjectURL:             projectURL,
						DownloadLink:           downloadLink,
						NewerVersions:          newerVersions,
						PendingSecurityPatches: pendingSecurityPatches,
					}
					model.DB.Create(&moduleEntry)
				}
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
}
