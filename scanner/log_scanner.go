package scanner

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/cheggaaa/pb"
	log "github.com/sirupsen/logrus"
	gormbulk "github.com/t-tiger/gorm-bulk-insert"
	"gitlab.com/codref/breach-scanner/model"
	"gitlab.com/codref/breach-scanner/util"
)

var logEntryBuffer []interface{}

// IndexPleskAccessLogs read a Plesk access log and indexes it
func IndexPleskAccessLogs(scanID uint, logFilePath string) {
	regexps := map[string]string{
		"pleskAccessLogRow": `(?m)^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+) - - \[(.*)\] "(GET|POST) (\/.*)\s(HTTP\/[0-9\.]+)"\s([0-9]+)\s([0-9]+)\s"(.*)"\s"(.*)"$`,
	}
	dateLayout := "02/Jan/2006:15:04:05 -0700"

	log.Infof("Processing file %s", logFilePath)

	// delete previous logs
	model.DB.Unscoped().Where("scan_id = ?", scanID).Delete(model.Log{})

	// open file
	file, err := os.Open(logFilePath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	totalLines, err := util.LineCounter(logFilePath)
	if err != nil {
		log.Fatal(err)
	}
	log.Infof("Found %d lines", totalLines)

	lines := 0
	bar := pb.StartNew(totalLines)

	// parse file line by line
	scanner := bufio.NewScanner(file)
	buf := make([]byte, 0, 1024*1024)
	scanner.Buffer(buf, 10*1024*1024)
	for scanner.Scan() {
		lines++
		bar.Increment()
		rowContent := scanner.Text()
		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}
		matches := util.StringSubmatchAll(regexps, rowContent)
		if len(matches["pleskAccessLogRow"]) > 0 {
			t, err := time.Parse(dateLayout, matches["pleskAccessLogRow"][1])
			if err != nil {
				fmt.Println(err)
			}
			length, err := strconv.Atoi(matches["pleskAccessLogRow"][6])
			if err != nil {
				fmt.Println(err)
			}
			logEntryBuffer = append(logEntryBuffer, model.Log{
				ScanID:           scanID,
				RowNumber:        uint(lines),
				IP:               matches["pleskAccessLogRow"][0],
				Date:             t,
				Method:           matches["pleskAccessLogRow"][2],
				URI:              matches["pleskAccessLogRow"][3],
				URIHash:          util.Md5(matches["pleskAccessLogRow"][3]),
				Type:             matches["pleskAccessLogRow"][4],
				Result:           matches["pleskAccessLogRow"][5],
				Length:           uint(length),
				Referer:          matches["pleskAccessLogRow"][7],
				ClientString:     matches["pleskAccessLogRow"][8],
				ClientStringHash: util.Md5(matches["pleskAccessLogRow"][8]),
			})
		}
		if len(logEntryBuffer) >= 30 {
			err = SaveLogEntries()
			if err != nil {
				log.Errorf("%s : while saving log entries", err)
			}
		}

	}

	bar.Finish()

	err = SaveLogEntries()
	if err != nil {
		log.Errorf("%s : while saving log entries", err)
	}

	log.Infof("Processed a total of %d lines", lines)
}

// SaveLogEntries saved log entries from buffer to database
func SaveLogEntries() (err error) {
	if len(logEntryBuffer) == 0 {
		log.Debugf("No logs in buffer")
		return
	}
	log.Debugf("Persisting %v log entries from buffer...", len(logEntryBuffer))
	err = gormbulk.BulkInsert(model.DB, logEntryBuffer, 100)
	// https://github.com/golang/go/wiki/CodeReviewComments#declaring-empty-slices
	logEntryBuffer = nil
	log.Debugf("Log entries' buffer persisted!")
	return
}
