package scanner

import (
	"os"
	"path"
	log "github.com/sirupsen/logrus"

	"github.com/mholt/archiver/v3"
	"gitlab.com/codref/breach-scanner/model"
	"gitlab.com/codref/breach-scanner/util"
)

// DownloadDruplaModules download all Drupal modules to a destination folder
func DownloadDruplaModules(scanID uint, destinationPath string, archivePoolPath string) {
	
	// Get all matched records
	var modules []model.Module
	model.DB.Where("scan_id = ?", scanID).Find(&modules)
	for _, module := range modules {
		if module.IsValid && module.DownloadLink != "" {
			fileName :=  path.Base(module.DownloadLink)
			filePath := archivePoolPath + "/" + fileName
			log.Infof("Downloading %s to %s...", fileName, archivePoolPath)
			if _, err := os.Stat(filePath); os.IsNotExist(err) {
				util.DownloadFile(module.DownloadLink, filePath)
				log.Info("File downloaded")	
			} else {
				log.Info("File already downloaded")	
			}
			
			log.Infof("Extracting %s to %s...", filePath, destinationPath)
			archiver.Unarchive(filePath, destinationPath)
			log.Info("File extracted")
		}
	}
}