package scanner

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/codref/breach-scanner/util"
)

// SplitMysqldumpFile split a commented mysqldump file to a set of table files
func SplitMysqldumpFile(mysqldumpFilePath string, outputPath string) {
	regexps := map[string]string{
		"tableName": "^\\s*--\\s*Table structure for table\\s*`(.*)`\\s*$",
	}

	log.Infof("Processing file %s", mysqldumpFilePath)
	log.Infof("Output folder is set to %s", outputPath)

	// open file
	file, err := os.Open(mysqldumpFilePath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// allocate cycle variables
	var tableFile *os.File
	var tableFilePath string
	defer tableFile.Close()
	lines := 0
	// parse file line by line
	scanner := bufio.NewScanner(file)
	buf := make([]byte, 0, 1024*1024)
	scanner.Buffer(buf, 10*1024*1024)
	for scanner.Scan() {
		lines++
		rowContent := scanner.Text()
		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}

		matches := util.StringSubmatchAll(regexps, rowContent)
		if len(matches["tableName"]) > 0 {
			if tableFile != nil {
				tableFile.Close()
			}
			tableFilePath := fmt.Sprintf("%s/%s.sql", outputPath, strings.Trim(matches["tableName"][0], " "))
			tableFile, err = os.OpenFile(tableFilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("Found table %s, saving into file %s\n", matches["tableName"][0], tableFilePath)
		}

		// add lines to tableFile
		if tableFile != nil {
			rowContent = strings.Replace(rowContent, "),(", "),\n(", -1)
			rowContent = strings.Replace(rowContent, " VALUES (", " VALUES \n(", -1)
			rowContent = strings.Replace(rowContent, ");\n", "),\n", -1)

			if _, err := tableFile.Write([]byte(rowContent)); err != nil {
				log.Fatalf("Cannot open output file %s. Error: %s", tableFilePath, err)
			}
			if _, err := tableFile.Write([]byte("\n")); err != nil {
				log.Fatalf("Cannot open output file %s. Error: %s", tableFilePath, err)
			}
		}

	}

	log.Infof("Processed a total of %d lines", lines)
}

// UnserializePHPString unserialize all modules configuration found on a SQL file
func UnserializePHPString(mysqldumpFilePath string, outputPath string) {
	regexps := map[string]string{
		"variableName": "^\\('([a-zA-Z0-9\\/\\._]+)',.*$",
		"moduleName":   "^\\('[a-zA-Z0-9\\/\\._]+','([a-zA-Z0-9_]+)',.*$",
		"serialized":   "^.*,('[asib]{1}:.*[};]{1}')\\).*$",
	}

	log.Infof("Processing file %s", mysqldumpFilePath)

	// open file
	file, err := os.Open(mysqldumpFilePath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	lines := 0
	// parse file line by line
	scanner := bufio.NewScanner(file)
	buf := make([]byte, 0, 1024*1024)
	scanner.Buffer(buf, 10*1024*1024)
	for scanner.Scan() {
		lines++
		rowContent := scanner.Text()
		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}

		matches := util.StringSubmatchAll(regexps, rowContent)
		if (len(matches["variableName"]) > 0 || len(matches["moduleName"]) > 0) && len(matches["serialized"]) > 0 {
			s := strings.Replace(matches["serialized"][0], "\\\"", "\"", -1)

			var name string
			if len(matches["moduleName"]) > 0 {
				name = strings.Trim(matches["moduleName"][0], " ")
			} else {
				name = strings.Trim(matches["variableName"][0], " ")
			}

			moduleFilePath := fmt.Sprintf("%s/%s.php", outputPath, name)

			moduleFile, err := os.OpenFile(moduleFilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("Found module %s, saving into file %s\n", name, moduleFilePath)

			if _, err := moduleFile.Write([]byte("<?php\n" + util.StringUnserializePHP(strings.Trim(s, "'")) + ";\n")); err != nil {
				log.Fatalf("Cannot open output file %s. Error: %s", moduleFilePath, err)
			}
		}
	}

	log.Infof("Processed a total of %d lines", lines)
}
