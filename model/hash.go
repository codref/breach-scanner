package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

// Hash is the output of a filesystem scan
type Hash struct {
	gorm.Model
	ScanID       uint   `gorm:"type:integer"`
	FilePath     string `gorm:"type:varchar(65535)"`
	Hash         string `gorm:"type:varchar(512)"`
	FileAccessed time.Time
	FileCreated  time.Time
	FileModified time.Time
}
