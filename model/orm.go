package model

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite" // this is required
)

// DB is globally exported factory of GORM ORM
var DB *gorm.DB

// ConnectToDatabase connects to database using sqlite dialect
func ConnectToDatabase(databasePath string) (err error) {
	DB, err = gorm.Open("sqlite3", databasePath)
	if err != nil {
		return
	}

	return DB.DB().Ping()
}

//FixDatabaseStructure ensures all tables, columns and indexes are in place
func FixDatabaseStructure() (err error) {
	err = DB.AutoMigrate(&Scan{}, &Hash{}, &Module{}, &Log{}).Error
	if err != nil {
		return
	}
	return
}
