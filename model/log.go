package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

// Log is the output of a Plesk log scan
type Log struct {
	gorm.Model
	ScanID           uint   `gorm:"type:integer"`
	FilePath         string `gorm:"type:varchar(65535)"`
	RowNumber        uint   `gorm:"type:integer"`
	IP               string
	Date             time.Time
	Method           string
	URI              string
	URIHash          string `gorm:"type:varchar(512)"`
	Type             string
	Result           string
	Length           uint
	Referer          string
	ClientString     string
	ClientStringHash string `gorm:"type:varchar(512)"`
}
