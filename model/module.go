package model

import (
	"github.com/jinzhu/gorm"
)

// Module is the output of a module scan
type Module struct {
	gorm.Model
	ScanID                 uint   `gorm:"type:integer"`
	Name                   string `gorm:"type:varchar(255)"`
	ProjectURL             string `gorm:"type:varchar(65535)"`
	ModulePath             string `gorm:"type:varchar(65535)"`
	Version                string `gorm:"type:varchar(64)"`
	IsValid                bool   `gorm:"type:bool"`
	DownloadLink           string `gorm:"type:varchar(65535)"`
	NewerVersions          int    `gorm:"type:integer"`
	PendingSecurityPatches int    `gorm:"type:integer"`
}
