package model

import (
	"github.com/jinzhu/gorm"
)

// Scan is saved scan
type Scan struct {
	gorm.Model
	Name            string `gorm:"type:varchar(100);unique_index"`
	HashIndexPath   string `gorm:"type:varchar(65535)"`
	ModuleIndexPath string `gorm:"type:varchar(65535)"`
}

// CreateScanID retrieve an ID of a scan, creates a new entry if not present in db
func CreateScanID(name string) Scan {
	var scan Scan
	DB.FirstOrCreate(&scan, Scan{Name: name})
	return scan
}

// GetScanID retrieve a scan ID
func GetScanID(name string) Scan {
	var scan Scan
	DB.First(&scan, Scan{Name: name})
	return scan
}
